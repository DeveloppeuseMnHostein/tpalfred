<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use PhpParser\Node\Expr\Cast\String_;

/**
 * Informations
 *
 * @ORM\Table(name="informations", indexes={@ORM\Index(name="fk_info_cat_idx", columns={"id_info_cat"}), @ORM\Index(name="fk_info_account_idx", columns={"id_info_account"})})
 * @ORM\Entity
 */
class Informations
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=120, nullable=false)
     */
    private ?string $name = "";

    /**
     * @var string|null
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private ?string $url = "";

    /**
     * @var string|null
     *
     * @ORM\Column(name="text", type="text", length=0, nullable=true)
     */
    private ?string $text = "";

    /**
     * @var string|null
     *
     * @ORM\Column(name="image", type="string", length=120, nullable=true)
     */
    private ?string $image = "";

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone", type="string", length=10, nullable=true)
     * 
     * @Assert\Regex(pattern="/[\+[0]]?[\+[1-7]]?[(]?[0-9]{8}[)]?[-\s\.]?$/", message="number_only")
     * 
     */
    private ?string $phone = "";

    /**
     * @var \Account
     *
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_info_account", referencedColumnName="id")
     * })
     */
    private $idInfoAccount;

    /**
     * @var \Categorytask
     *
     * @ORM\ManyToOne(targetEntity="Categorytask")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_info_cat", referencedColumnName="id")
     * })
     */
    private $idInfoCat;
}
