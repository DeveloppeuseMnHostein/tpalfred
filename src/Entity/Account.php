<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Account
 *
 * @ORM\Table(name="account")
 * @ORM\Entity
 */
class Account
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=45, nullable=false)
     * 
     * @Assert\Length(
     *      min = 2,
     *      max = 45,
     *      minMessage = "Your login must be at least {{ limit }} characters long",
     *      maxMessage = "Your login cannot be longer than {{ limit }} characters"
     * )
     * 
     * @Assert\Regex(
     * pattern="/^[a-z_-çéè]{3,15}$/",
     * message="not_valid_name"
     * )
     * 
     */
    private ?string $login = "";

    /**
     * @var string
     *
     * @ORM\Column(name="mail", type="string", length=120, nullable=false)
     * 
     * @ORM\Column(
     *      name="email", 
     *      type="string", 
     *      length=320, 
     *      nullable=false, 
     *      options={
     *          "comment"="Length >= 8, regex (email)"
     *      }
     * )
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email."
     * )
     * 
     */
    private ?string $mail = "";

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=45, nullable=false)
     * 
     * @Assert\Length(
     *      min = 8,
     *      max = 45,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     * @Assert\Regex(
     * pattern="/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!.@$ %^&*-]).{8,}$/",
     * message="not_valid_password"
     * )
     * 
     */
    private ?string $password = "";
}
