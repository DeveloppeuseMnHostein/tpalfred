<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Todolist
 *
 * @ORM\Table(name="todolist", indexes={@ORM\Index(name="fk_cattask_idx", columns={"id_cattask"}), @ORM\Index(name="fk_account_idx", columns={"id_account"})})
 * @ORM\Entity
 */
class Todolist
{
    /**
     * @var int
     *
     * @ORM\Column(name=" id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     */
    private ?string $name = "";

    /**
     * @var \Account
     *
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_account", referencedColumnName="id")
     * })
     */
    private $idAccount;

    /**
     * @var \Categorytask
     *
     * @ORM\ManyToOne(targetEntity="Categorytask")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cattask", referencedColumnName="id")
     * })
     */
    private $idCattask;
}
