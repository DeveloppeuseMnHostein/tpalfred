<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Useridentity
 *
 * @ORM\Table(name="useridentity", uniqueConstraints={@ORM\UniqueConstraint(name="id_user_UNIQUE", columns={"id_user"})}, indexes={@ORM\Index(name="fk_languages_idx", columns={"id_language"}), @ORM\Index(name="fk_timezone_idx", columns={"id_timezone"})})
 * @ORM\Entity
 */
class Useridentity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     * 
     * @Assert\Length(
     *      min = 2,
     *      max = 45,
     *      minMessage = "Your name must be at least {{ limit }} characters long",
     *      maxMessage = "Your name cannot be longer than {{ limit }} characters"
     * )
     * 
     */
    private ?string $name = "";

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=45, nullable=false)
     * 
     * @Assert\Length(
     *      min = 2,
     *      max = 45,
     *      minMessage = "Your name must be at least {{ limit }} characters long",
     *      maxMessage = "Your name cannot be longer than {{ limit }} characters"
     * )
     * 
     */
    private ?string $surname = "";

    /**
     * @var string
     *
     * @ORM\Column(name="portrait", type="string", length=250, nullable=false, options={"comment"="url"})
     */
    private ?string $portrait = "";

    /**
     * @var \Account
     *
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;

    /**
     * @var \Timezone
     *
     * @ORM\ManyToOne(targetEntity="Timezone")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_timezone", referencedColumnName="id")
     * })
     */
    private $idTimezone;

    /**
     * @var \Languages
     *
     * @ORM\ManyToOne(targetEntity="Languages")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_language", referencedColumnName="id")
     * })
     */
    private $idLanguage;
}
