<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categorytask
 *
 * @ORM\Table(name="categorytask")
 * @ORM\Entity
 */
class Categorytask
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=120, nullable=false)
     */
    private ?string $name = "";
}
