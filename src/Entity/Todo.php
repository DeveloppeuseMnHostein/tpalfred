<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Todo
 *
 * @ORM\Table(name="todo", indexes={@ORM\Index(name="fk_list_idx", columns={"id_list"}), @ORM\Index(name="fk_alarm_todo_idx", columns={"id_alarm_todo"})})
 * @ORM\Entity
 */
class Todo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     */
    private ?string $name = "";

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=500, nullable=false)
     */
    private ?string $description = "";

    /**
     * @var int|null
     *
     * @ORM\Column(name="id_alarm_todo", type="integer", nullable=true)
     */
    private $idAlarmTodo;

    /**
     * @var \Todolist
     *
     * @ORM\ManyToOne(targetEntity="Todolist")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_list", referencedColumnName=" id")
     * })
     */
    private $idList;
}
