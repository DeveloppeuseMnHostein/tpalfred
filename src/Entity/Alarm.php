<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Alarm
 *
 * @ORM\Table(name="alarm", indexes={@ORM\Index(name="fk_alarm_idx", columns={"id_alarm"}), @ORM\Index(name="fk_alarm_event_idx", columns={"id_event"})})
 * @ORM\Entity
 */
class Alarm
{
    /**
     * @var int
     *
     * @ORM\Column(name=" id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     * 
     * @Assert\Length(
     *      min = 2,
     *      max = 45,
     *      minMessage = "Your name must be at least {{ limit }} characters long",
     *      maxMessage = "Your name cannot be longer than {{ limit }} characters"
     * )
     * 
     */
    private ?string $name = "";

    /**
     * @var string
     *
     * @ORM\Column(name="recurrence", type="string", length=45, nullable=false)
     */
    private ?string $recurrence = "";

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alarm_time", type="datetime", nullable=false)
     */
    private ?\Datetime $alarmTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="alarm_day", type="datetime", nullable=false)
     */
    private ?\DateTime $alarmDay;

    /**
     * @var \Event
     *
     * @ORM\ManyToOne(targetEntity="Event")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_alarm", referencedColumnName="id")
     * })
     */
    private $idAlarm;

    /**
     * @var \Event
     *
     * @ORM\ManyToOne(targetEntity="Event")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_event", referencedColumnName="id")
     * })
     */
    private $idEvent;
}
