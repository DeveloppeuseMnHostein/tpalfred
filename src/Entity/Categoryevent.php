<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categoryevent
 *
 * @ORM\Table(name="categoryevent")
 * @ORM\Entity
 */
class Categoryevent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     */
    private ?string $name = "";
}
