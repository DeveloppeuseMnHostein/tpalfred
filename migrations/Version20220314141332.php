<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220314141332 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE alarm DROP FOREIGN KEY fk_alarm');
        $this->addSql('ALTER TABLE alarm DROP FOREIGN KEY fk_alarm_event');
        $this->addSql('ALTER TABLE alarm CHANGE id_event id_event INT DEFAULT NULL, CHANGE id_alarm id_alarm INT DEFAULT NULL');
        $this->addSql('ALTER TABLE alarm ADD CONSTRAINT FK_749F46DD9A1A07ED FOREIGN KEY (id_alarm) REFERENCES event (id)');
        $this->addSql('ALTER TABLE alarm ADD CONSTRAINT FK_749F46DDD52B4B97 FOREIGN KEY (id_event) REFERENCES event (id)');
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY fk_event_account');
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY fk_event_cat');
        $this->addSql('ALTER TABLE event CHANGE id_event_account id_event_account INT DEFAULT NULL, CHANGE id_event_cat id_event_cat INT DEFAULT NULL');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA746492E6E FOREIGN KEY (id_event_account) REFERENCES account (id)');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA7FA43A5C FOREIGN KEY (id_event_cat) REFERENCES categoryevent (id)');
        $this->addSql('ALTER TABLE informations CHANGE id_info_account id_info_account INT DEFAULT NULL, CHANGE id_info_cat id_info_cat INT DEFAULT NULL');
        $this->addSql('ALTER TABLE todo DROP FOREIGN KEY fk_alarm_todo');
        $this->addSql('ALTER TABLE todo CHANGE id_alarm_todo id_alarm_todo INT DEFAULT NULL, CHANGE id_list id_list INT DEFAULT NULL');
        $this->addSql('ALTER TABLE todo ADD CONSTRAINT FK_5A0EB6A0D0A7B95A FOREIGN KEY (id_alarm_todo) REFERENCES alarm ( id)');
        $this->addSql('ALTER TABLE todolist DROP FOREIGN KEY fk_account');
        $this->addSql('ALTER TABLE todolist DROP FOREIGN KEY fk_cattask');
        $this->addSql('ALTER TABLE todolist CHANGE id_account id_account INT DEFAULT NULL, CHANGE id_cattask id_cattask INT DEFAULT NULL');
        $this->addSql('ALTER TABLE todolist ADD CONSTRAINT FK_DD4DF6DBA3ABFFD4 FOREIGN KEY (id_account) REFERENCES account (id)');
        $this->addSql('ALTER TABLE todolist ADD CONSTRAINT FK_DD4DF6DB456FBEBF FOREIGN KEY (id_cattask) REFERENCES categorytask (id)');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY fk_id_user');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY fk_timezone');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY fk_languages');
        $this->addSql('ALTER TABLE user CHANGE id_user id_user INT DEFAULT NULL, CHANGE id_language id_language INT DEFAULT NULL, CHANGE id_timezone id_timezone INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6496B3CA4B FOREIGN KEY (id_user) REFERENCES account (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64967DA5E02 FOREIGN KEY (id_timezone) REFERENCES timezone (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64984009D20 FOREIGN KEY (id_language) REFERENCES languages (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE messenger_messages');
        $this->addSql('ALTER TABLE account CHANGE password password VARCHAR(45) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE mail mail VARCHAR(120) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE login login VARCHAR(45) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE alarm DROP FOREIGN KEY FK_749F46DD9A1A07ED');
        $this->addSql('ALTER TABLE alarm DROP FOREIGN KEY FK_749F46DDD52B4B97');
        $this->addSql('ALTER TABLE alarm CHANGE id_alarm id_alarm INT NOT NULL, CHANGE id_event id_event INT NOT NULL, CHANGE name name VARCHAR(45) NOT NULL COLLATE `utf8mb4_unicode_520_ci`, CHANGE recurrence recurrence VARCHAR(45) DEFAULT NULL COLLATE `utf8mb4_unicode_520_ci`');
        $this->addSql('ALTER TABLE alarm ADD CONSTRAINT fk_alarm FOREIGN KEY (id_alarm) REFERENCES event (id) ON UPDATE CASCADE');
        $this->addSql('ALTER TABLE alarm ADD CONSTRAINT fk_alarm_event FOREIGN KEY (id_event) REFERENCES event (id) ON UPDATE CASCADE');
        $this->addSql('ALTER TABLE categoryevent CHANGE name name VARCHAR(45) NOT NULL COLLATE `utf8mb4_unicode_520_ci`');
        $this->addSql('ALTER TABLE categorytask CHANGE name name VARCHAR(120) NOT NULL COLLATE `utf8mb4_unicode_520_ci`');
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA746492E6E');
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA7FA43A5C');
        $this->addSql('ALTER TABLE event CHANGE id_event_account id_event_account INT NOT NULL, CHANGE id_event_cat id_event_cat INT NOT NULL, CHANGE name name VARCHAR(45) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(500) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE place place VARCHAR(120) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT fk_event_account FOREIGN KEY (id_event_account) REFERENCES account (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT fk_event_cat FOREIGN KEY (id_event_cat) REFERENCES categoryevent (id) ON UPDATE CASCADE');
        $this->addSql('ALTER TABLE informations CHANGE id_info_account id_info_account INT NOT NULL, CHANGE id_info_cat id_info_cat INT NOT NULL, CHANGE name name VARCHAR(120) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE url url VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE text text LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE image image VARCHAR(120) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE phone phone VARCHAR(10) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE languages CHANGE name name VARCHAR(45) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE timezone CHANGE name name VARCHAR(45) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE todo DROP FOREIGN KEY FK_5A0EB6A0D0A7B95A');
        $this->addSql('ALTER TABLE todo CHANGE id_alarm_todo id_alarm_todo INT NOT NULL, CHANGE id_list id_list INT NOT NULL, CHANGE name name VARCHAR(45) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(500) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE todo ADD CONSTRAINT fk_alarm_todo FOREIGN KEY (id_alarm_todo) REFERENCES alarm ( id) ON UPDATE CASCADE');
        $this->addSql('ALTER TABLE todolist DROP FOREIGN KEY FK_DD4DF6DBA3ABFFD4');
        $this->addSql('ALTER TABLE todolist DROP FOREIGN KEY FK_DD4DF6DB456FBEBF');
        $this->addSql('ALTER TABLE todolist CHANGE id_account id_account INT NOT NULL, CHANGE id_cattask id_cattask INT NOT NULL, CHANGE name name VARCHAR(45) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE todolist ADD CONSTRAINT fk_account FOREIGN KEY (id_account) REFERENCES account (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE todolist ADD CONSTRAINT fk_cattask FOREIGN KEY (id_cattask) REFERENCES categorytask (id) ON UPDATE CASCADE');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6496B3CA4B');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64967DA5E02');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64984009D20');
        $this->addSql('ALTER TABLE user CHANGE id_user id_user INT NOT NULL, CHANGE id_timezone id_timezone INT NOT NULL, CHANGE id_language id_language INT NOT NULL, CHANGE name name VARCHAR(45) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE surname surname VARCHAR(45) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE portrait portrait VARCHAR(250) NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'url\'');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT fk_id_user FOREIGN KEY (id_user) REFERENCES account (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT fk_timezone FOREIGN KEY (id_timezone) REFERENCES timezone (id) ON UPDATE CASCADE');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT fk_languages FOREIGN KEY (id_language) REFERENCES languages (id) ON UPDATE CASCADE ON DELETE NO ACTION');
    }
}
